const express = require('express')
const app = express();
var path = require('path');

var WebSocketServer = require('ws').Server;

var ws = new WebSocketServer({ port: 8080 });
config = {
  port: 6379,
  host: 'ccf-20210531084348531-ms.cn-aqi6qrvcrew2zmpitnru6m06-ccf',
  password: 'Macrock!2',
};
var redis2 = require('redis').createClient(config);
var redisSchedulerOver = require('redis').createClient(config);
var redispublish = require('redis').createClient(config);
var redisstoppublish = require('redis').createClient(config);
var redisstoppublishnew = require('redis').createClient(config);
var redisesttime = require('redis').createClient(config);
var redisupdatestatus = require('redis').createClient(config);
var redislogs = require('redis').createClient(config);
var redisbatch = require('redis').createClient(config);


var client = require('redis').createClient(config);
redis2.subscribe('schedulerlog');
redislogs.subscribe('predictionlog')
redisSchedulerOver.subscribe('schedulerover');
redisstoppublish.subscribe('setstopjob');
redisstoppublish.subscribe('stopbtntrue');
redisstoppublish.subscribe('clearcomdatarep');
redisstoppublishnew.subscribe('statusnew');
redisupdatestatus.subscribe('statusdbupdated');
redisesttime.subscribe('esttime')
redisbatch.subscribe('endbatch')


ws.on('connection', function connection(ws) {
  redis2.on('message', function (channel, data) {
    ws.send(JSON.stringify({ "data": data, "channel": channel }));
  });
  redislogs.on('message', function (channel, data) {
    ws.send(JSON.stringify({ "data": data, "channel": channel }));
  });

  redisstoppublish.on('message', function (channel, data) {
    ws.send(JSON.stringify({ "data": data, "channel": channel }));
  });

  redisesttime.on('message', function (channel, data) {
    ws.send(JSON.stringify({ "data": data, "channel": channel }));
  });

  redisbatch.on('message', function (channel, data) {
    ws.send(JSON.stringify({ "data": data, "channel": channel }));
  });

  redisupdatestatus.on('message', function (channel, data) {
    client.hgetall("statushash", function (err, newdata) {
      ws.send(JSON.stringify({ "data": newdata, "channel": channel }));
    });
  });

  ws.on('message', function incoming(message) {
    var newmsg = message.split('-')
    if (newmsg[1] == "canceljob") {
      var txt1 = newmsg[0] + "-true"
      var txt2 = newmsg[0] + "-cancel"
      redispublish.publish('stopbtntrue', txt1)
      redispublish.publish('cancelprocess', txt2);
    }
    else if (newmsg[0] === "allstatuses") {
      client.hgetall("statushash", function (err, newdata) {
        ws.send(JSON.stringify({ "data": newdata, "channel": "allstatuses" }));
      });
    }
    else if (newmsg[1] == "clearcomdata") {
      var txt = newmsg[0] + '-true'
      redispublish.publish('clearcomdatarep', txt);
    }
    else if (newmsg[0] == "jobstatus") {
      var data = JSON.parse(newmsg[1])
      for (var key in data) {
        client.hset('statushash', key, data[key])
        redispublish.publish('statusdbupdated', "test");
      }

    } else if (newmsg[0] == "alllog") {
      redispublish.publish('allsub', newmsg[1]);
    } else if (newmsg[0] == "login") {
      redispublish.publish('loginlog', newmsg[1]);
    }
  });

  redisSchedulerOver.on('message', function (channel, data) {
    ws.send(JSON.stringify({ "data": data, "channel": channel }));
  });

});
   